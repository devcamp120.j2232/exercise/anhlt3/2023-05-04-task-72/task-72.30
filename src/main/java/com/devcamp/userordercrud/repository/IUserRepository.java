package com.devcamp.userordercrud.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.devcamp.userordercrud.model.CUser;

public interface IUserRepository extends CrudRepository<CUser, Long> {

    Page<CUser> findAll(Pageable pageWithFiveElements);

}
