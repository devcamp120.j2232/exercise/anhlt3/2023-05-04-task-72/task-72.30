package com.devcamp.userordercrud.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.userordercrud.model.CUser;
import com.devcamp.userordercrud.repository.IUserRepository;

@RestController
@CrossOrigin(value = "*", maxAge = -1)
@RequestMapping("/")
public class UserController {
    @Autowired
    IUserRepository pUserRepository;

    @GetMapping("/users")
    public ResponseEntity<Object> getAllUsers() {
        List<CUser> userList = new ArrayList<CUser>();
        pUserRepository.findAll().forEach(userElement -> {
            userList.add(userElement);
        });

        if (!userList.isEmpty()) {
            return new ResponseEntity<Object>(userList, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable(name = "id") Long id) {
        Optional<CUser> userFounded = pUserRepository.findById(id);
        if (userFounded.isPresent()) {
            return new ResponseEntity<Object>(userFounded, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@RequestBody CUser userFromClient) {
        try {
            CUser _user = new CUser(userFromClient.getFullName(), userFromClient.getEmail(), userFromClient.getPhone(),
                    userFromClient.getAddress());
            Date _now = new Date();
            _user.setCreated(_now);
            _user.setUpdated(null);
            pUserRepository.save(_user);
            return new ResponseEntity<Object>(_user, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Fail to Create specified user: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable(name = "id") Long id, @RequestBody CUser userUpdate) {
        Optional<CUser> _userData = pUserRepository.findById(id);
        if (_userData.isPresent()) {
            CUser _user = _userData.get();
            _user.setFullName(userUpdate.getFullName());
            _user.setEmail(userUpdate.getEmail());
            _user.setPhone(userUpdate.getPhone());
            _user.setAddress(userUpdate.getAddress());
            _user.setUpdated(new Date());
            try {
                return ResponseEntity.ok(pUserRepository.save(_user));
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not excute operation of this Entity: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable("id") Long id) {
        Optional<CUser> _userData = pUserRepository.findById(id);
        if (_userData.isPresent()) {
            try {
                pUserRepository.deleteById(id);
                return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not excute operation of this Entity: " + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<Object>("User not found!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users-count")
    public long countUser() {
        return pUserRepository.count();
    }

    @GetMapping("/users/check/{id}")
    public boolean checkUserById(@PathVariable Long id) {
        return pUserRepository.existsById(id);
    }

    @GetMapping("/user5")
    public ResponseEntity<List<CUser>> getFiveUser(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "5") int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<CUser> userPage = pUserRepository.findAll(pageable);
            List<CUser> users = userPage.getContent();
            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
